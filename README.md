# pipeline-definition

YAMLs for CKI pipeline definition

Python tests can be run with `pytest`.

# Pipeline testing

The preferred way is to make an MR and ask `cki-bot` to test the changes. This
reruns the *last successful pipelines* with your branch/repo.

In case you have to run with a specific pipeline that's not the last successful,
use `retrigger` from [cki-tools](https://gitlab.com/cki-project/cki-tools/).
This is what `cki-bot` uses on the background too. You need to have valid tokens
to use this method.

The last way is to manually trigger a pipeline (e.g. via `curl`). **Don't do
this unless you are 150% sure you know what you're doing and which variables
need to be overriden and why and you can recite this from your sleep!!!** Using
the full set of original variables from production pipelines may and will break
various parts. This includes (but is not limited to) patch checker sending
duplicate emails for previously checked series, reporter sending out "your
patch/build is broken" emails, sending of broken gating UMB messages and broken
patch trigger. Our production and testing environment is the same so really, be
careful when doing this. All variable overrides are already handled by the
`retrigger` tool so just use that if you can.

